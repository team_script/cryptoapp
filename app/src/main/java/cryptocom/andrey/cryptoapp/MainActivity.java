package cryptocom.andrey.cryptoapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Objects;

import cryptocom.andrey.cryptoapp.security.DesEncrypter;
import cryptocom.andrey.cryptoapp.security.Encrypter;
import cryptocom.andrey.cryptoapp.security.RC4;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Spinner spCryptoMod;
    Spinner spKeySize;
    Button btFile;
    Button btDo;
    RadioButton btEncode;
    RadioButton btDecode;
    EditText etPassword;
    TextView tvFileName;
    File file;

    private static final int FILE_SELECT_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spCryptoMod = (Spinner) findViewById(R.id.spCryptoMod);
        spKeySize = (Spinner) findViewById(R.id.spKeySize);
        btFile = (Button) findViewById(R.id.btFile);
        btDo = (Button) findViewById(R.id.btDo);
        btEncode = (RadioButton) findViewById(R.id.btEncode);
        btDecode = (RadioButton) findViewById(R.id.btDecode);
        etPassword = (EditText) findViewById(R.id.etPassword);
        tvFileName = (TextView) findViewById(R.id.tvFileName);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.CryptoMode, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCryptoMod.setAdapter(adapter);
        spCryptoMod.setSelection(0);

        btFile.setOnClickListener(this);
        btDo.setOnClickListener(this);
    }


    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    // Get the path
                    try {
                        String path = FileUtils.getPath(this, uri);
                        file = new File(path);
                        visibleFileName(file.getPath());
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    String readFile(File file) {
        String str = "";
        try {
            /*BufferedReader br = new BufferedReader(new FileReader(file));
            str = br.readLine();*/
            FileInputStream fIn = new FileInputStream(file.getPath());
            InputStreamReader isr = new InputStreamReader(fIn);
            char[] inputBuffer = new char[fIn.available()];
            isr.read(inputBuffer);
            str = new String(inputBuffer);
            Log.d("kaka", str);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }

    void writeFileSD(String _name, String _file) {
        // проверяем доступность SD
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.d("kaka", "Память не доступна: " + Environment.getExternalStorageState());
            return;
        }
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        // добавляем свой каталог к пути
        String path = sdPath.getAbsolutePath() + "/";
        sdPath = new File(path);
        // создаем каталог
        sdPath.mkdirs();
        // формируем объект File, который содержит путь к файлу
        File sdFile = new File(sdPath, _name);
        try {
            // открываем поток для записи
            BufferedWriter bw = new BufferedWriter(new FileWriter(sdFile));
            // пишем данные
            bw.write(_file);
            // закрываем поток
            bw.close();
            Log.d("kaka", "Файл записан на SD: " + sdFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void visibleFileName(String name) {
        tvFileName.setVisibility(View.VISIBLE);
        tvFileName.setText(name);

    }

    void DESaction() {
        DesEncrypter des = new DesEncrypter(etPassword.getText().toString());
        if (btEncode.isChecked()) {
            Log.d("kaka", des.encrypt(readFile(file)));
            writeFileSD("encryptedDES_" + file.getName(), des.encrypt(readFile(file)));
            Toast.makeText(getApplicationContext(),
                    file.getName() + " зашифрован DES", Toast.LENGTH_SHORT).show();
        } else {
            String temp = des.decrypt(readFile(file));
            if (temp == null) {
                Toast.makeText(getApplicationContext(), "Не правильная кодовая фраза", Toast.LENGTH_SHORT).show();
            } else {
                writeFileSD("decryptedDES_" + file.getName(), temp);
                Toast.makeText(getApplicationContext(),
                        file.getName() + " расшифрован", Toast.LENGTH_SHORT).show();
            }
        }
    }

    void TripleDESaction() {
        Encrypter tripleDes = new Encrypter(etPassword.getText().toString());
        if(btEncode.isChecked()) {
            writeFileSD("encrypted3DES_" + file.getName(), tripleDes.encrypt(readFile(file)));
            Toast.makeText(getApplicationContext(), file.getName() + "зашифрован 3DES", Toast.LENGTH_SHORT).show();
        } else {
            String temp = tripleDes.decrypt(readFile(file));
            if (temp == null) {
                Toast.makeText(getApplicationContext(), "Не правильная кодовая фраза ", Toast.LENGTH_SHORT).show();
            } else {
                writeFileSD("decrypted3DES_" + file.getName(), temp);
                Toast.makeText(getApplicationContext(), file.getName() + " расшифрован 3DES", Toast.LENGTH_SHORT).show();
            }
        }
    }

    void RC4action() throws Exception {
        RC4 rc4 = new RC4(etPassword.getText().toString().getBytes("UTF-8"));
        if(btEncode.isChecked()){
            String temp = Base64.encodeToString(rc4.encrypt(readFile(file).getBytes()), Base64.DEFAULT);
            writeFileSD("encryptedRC4_" + file.getName(), temp);
            Toast.makeText(getApplicationContext(), file.getName() + "зашифрован RC4", Toast.LENGTH_SHORT).show();
        }else{
            String temp = new String(rc4.decrypt(Base64.decode(readFile(file), Base64.DEFAULT)));
            if (temp == null) {
                Toast.makeText(getApplicationContext(), "Не правильная кодовая фраза ", Toast.LENGTH_SHORT).show();
            } else {
                writeFileSD("decryptedRC4_" + file.getName(), temp);
                Toast.makeText(getApplicationContext(), file.getName() + " расшифрован RC4", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btFile:
                showFileChooser();
                break;
            case R.id.btDo:
                if (!(Objects.equals(etPassword.getText().toString(), "")) && !(tvFileName.getText() == "")) {
                    switch (spCryptoMod.getSelectedItemPosition()) {
                        case 0:
                            DESaction();
                            break;
                        case 1:
                            TripleDESaction();
                            break;
                        case 2:
                            try {
                                RC4action();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;

                    }
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Выберите файл или введите пароль", Toast.LENGTH_SHORT);
                    toast.show();
                }

                break;
        }
    }
}
