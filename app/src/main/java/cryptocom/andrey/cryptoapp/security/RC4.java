package cryptocom.andrey.cryptoapp.security;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * Created by Andrey on 17.03.2017.
 */

public class RC4 {
    private final byte[] S = new byte[256];
    private final byte[] T = new byte[256];
    private final int keyLength;

    public RC4(final byte[] _key) {
        if (_key.length < 1 || _key.length > 256) {
            throw new IllegalArgumentException("Ключ должен быть меньше 256 байт и больше 1");
        } else {
            keyLength = _key.length;
            for (int i = 0; i < 256; i++) {
                S[i] = (byte) i;
                T[i] = _key[i % keyLength];
            }
            int j = 0;
            byte tmp;
            for (int i = 0; i < 256; i++) {
                j = (j + S[i] + T[i]) & 0xFF;
                tmp = S[j];
                S[j] = S[i];
                S[i] = tmp;
            }
        }
    }

    public byte[] encrypt(final byte[] plainText)  {
        final byte[] cipherText = new byte[plainText.length];
        int i = 0, j = 0, k, t;
        byte tmp;
        for (int counter = 0; counter < plainText.length; counter++) {
            i = (i + 1) & 0xFF;
            j = (j + S[i]) & 0xFF;
            tmp = S[j];
            S[j] = S[i];
            S[i] = tmp;
            t = (S[i] + S[j]) & 0xFF;
            k = S[t];
            cipherText[counter] = (byte) (plainText[counter] ^ k);
        }
        return cipherText;
    }

    public byte[] decrypt(byte[] cipherText) {
        return encrypt(cipherText);
    }
}
